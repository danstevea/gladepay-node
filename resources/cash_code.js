"use strict";

var route = "/bills";

module.exports = {
  /**
   * Generate Cashcode
   */
  generateCashcode: {
    method: "put",
    route: route,
    params: ["amount*", "pin*", "channel*", "orderRef*", "life_time*"],
    data: {
      action: "generate",
    },
  },
  /**
   * Verify Cashcode
   */
  verifyCashcode: {
    method: "put",
    route: route,
    params: ["orderRef*"],
    data: {
      action: "verify",
    },
  },
  /**
   * Cancel Cashcode
   */
  cancelCashcode: {
    method: "put",
    route: route,
    params: ["txnRef*"],
    data: {
      action: "cancel",
    },
  },
};
