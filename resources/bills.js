"use strict";

var route = "/bills";

module.exports = {
  /**
   * Get List of Bills
   */
  billList: {
    method: "put",
    route: route,
    params: ["category", "bills_id"],
    data: {
      action: "pull",
    },
  },
  /**
   * Pay Bills
   */
  payBill: {
    method: "put",
    route: route,
    params: ["paycode*", "reference*", "amount*", "orderRef*"],
    data: {
      action: "pull",
    },
  },
  /**
   * Resolve Bills Name
   */
  resolveBillsName: {
    method: "put",
    route: route,
    params: ["paycode*", "reference*", "amount"],
    data: {
      action: "resolve",
    },
  },
  /**
   * Verify Bills
   */
  verifyBills: {
    method: "put",
    route: route,
    params: ["txnRef*"],
    data: {
      action: "verify",
    },
  },
};
