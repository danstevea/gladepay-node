"use strict";

var route = "/payment";

module.exports = {
  /**
   * One-off payment
   */
  cardPayment: {
    method: "put",
    route: route,
    params: ["user*", "card*", "amount*", "country*", "currency*"],
    data: {
      action: "initiate",
      paymentType: "card",
    },
  },

  /**
   * Recurrent
   */
  recurrent: {
    method: "put",
    route: route,
    params: [
      "user*",
      "card*",
      "amount*",
      "country*",
      "currency*",
      "recurrent*",
    ],
    data: {
      action: "initiate",
      paymentType: "card",
    },
  },

  /**
   * Installment
   */
  installment: {
    method: "put",
    route: route,
    params: [
      "user*",
      "card*",
      "amount*",
      "country*",
      "currency*",
      "installment*",
    ],
    data: {
      action: "initiate",
      paymentType: "card",
    },
  },

  /**
   * Charge card
   */
  chargeCard: {
    method: "put",
    route: route,
    params: [
      "user*",
      "card*",
      "amount*",
      "country*",
      "currency*",
      "txnRef*",
      "auth_type",
    ],
    data: {
      action: "charge",
      paymentType: "card",
    },
  },

  /**
   * Charge token
   */
  chargeToken: {
    method: "put",
    route: route,
    params: ["token*", "user*", "amount*"],
    data: {
      action: "charge",
      paymentType: "token",
    },
  },

  /**
   * OTP Validation
   */
  validateOTP: {
    method: "put",
    route: route,
    params: ["txnRef*", "otp*"],
    data: {
      action: "validate",
    },
  },

  /**
   * Verify transaction
   */
  verify: {
    method: "put",
    route: route,
    params: ["txnRef*"],
    data: {
      action: "verify",
    },
  },

  /**
   * Account payment
   */
  accountPayment: {
    method: "put",
    route: route,
    params: ["user*", "account*", "amount*"],
    data: {
      action: "charge",
      paymentType: "account",
    },
  },

  /**
   * Validate account payment
   */
  validateAccount: {
    method: "put",
    route: route,
    params: ["txnRef*", "otp*"],
    data: {
      action: "validate",
      validate: "account",
    },
  },

  /**
   * Pay with phone number
   */
  payWithPhoneNumber: {
    method: "put",
    route: route,
    params: ["user*", "phonenumber*", "amount*", "country*", "currency*"],
    data: {
      action: "charge",
      paymentType: "phone",
    },
  },
  /**
   * Charge Ussd
   */
  chargeUSSD: {
    method: "put",
    route: route,
    params: ["bank*", "phonenumber*", "accountnumber*", "user*", "amount*"],
    data: {
      action: "charge",
      paymentType: "ussd",
    },
  },
  /**
   * Charge Qrpay
   */
  chargeQrpay: {
    method: "put",
    route: route,
    params: ["user*", "amount*", "business_name"],
    data: {
      action: "charge",
      paymentType: "qrpay",
    },
  },
  /**
   * Charge Wallet
   */
  chargeWallet: {
    method: "put",
    route: route,
    params: ["user*", "amount*", "wallet*"],
    data: {
      action: "charge",
      paymentType: "wallet",
    },
  },
  /**
   * Wallet Validation
   */
  validateWallet: {
    method: "put",
    route: route,
    params: ["txnRef*", "otp*"],
    data: {
      action: "validate",
      validate: "wallet",
    },
  },
  /**
   * Refund Transactions
   */
  refundTransactions: {
    method: "put",
    route: route,
    params: ["txnRef*"],
    data: {
      action: "verify",
    },
  },
};
